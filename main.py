import random
"""
Ce script Python effectue plusieurs opérations de tri et d'affichage sur une liste de données. Il utilise la bibliothèque 'random' pour mélanger la liste initiale.

Paramètres:
    liste (list): liste contenant des données sous forme de listes imbriquées, chaque sous-liste contenant le prénom (str), le nom (str) et l'âge (int) d'une personne.
    titre (str): Le titre à afficher avant d'afficher la liste.

Fonctions:
    afficher_liste(liste, titre):
        Affiche la liste donnée sous forme de texte avec un titre.

Opérations:
    1) Trie la liste par ordre alphabétique des noms.
    2) Trie la liste par ordre alphabétique inversé des prénoms.
    3) Trie la liste par ordre croissant des âges.
    4) Trie la liste par ordre alphabétique des noms, puis des prénoms en cas de noms identiques.
    5) Mélange aléatoirement la liste initiale.

Exemple d'utilisation:
    # Déclaration de la liste initiale
    liste = [
        ["Pascal", "ROSE", 43],
        ["Mickaël", "FLEUR", 29],
        # ...
    ]

    # Trie la liste par ordre alphabétique des noms
    liste_1 = sorted(liste, key=lambda x: x[1])
    afficher_liste(liste_1, "Liste 1 : tri par ordre alphabétique des noms")

    # Autres opérations de tri et d'affichage...
"""
# Déclaration de la liste initiale
liste = [
    ["Pascal", "ROSE", 43],
    ["Mickaël", "FLEUR", 29],
    ["Henri", "TULIPE", 35],
    ["Michel", "FRAMBOISE", 35],
    ["Arthur", "PETALE", 35],
    ["Michel", "POLLEN", 50],
    ["Maurice", "FRAMBOISE", 42]
]

# Fonction pour afficher une liste sous forme de texte
def afficher_liste(liste, titre):
    print(f"## {titre} ##")
    for item in liste:
        print(f"{item[0]} {item[1]} {item[2]} ans")
    print("## FIN ##")

# 1) Trier par ordre alphabétique des noms
liste_1 = sorted(liste, key=lambda x: x[1])
afficher_liste(liste_1, "Liste 1 : tri par ordre alphabétique des noms")

# 2) Trier par ordre alphabétique inversé des prénoms
liste_2 = sorted(liste, key=lambda x: x[0], reverse=True)
afficher_liste(liste_2, "Liste 2 : tri par ordre alphabétique inversé des prénoms")

# 3) Trier par ordre croissant des âges
liste_3 = sorted(liste, key=lambda x: x[2])
afficher_liste(liste_3, "Liste 3 : tri par ordre croissant des âges")

# 4) Trier par ordre alphabétique des noms, puis des prénoms en cas de noms identiques
liste_4 = sorted(liste, key=lambda x: (x[1], x[0]))
afficher_liste(liste_4, "Liste 4 : tri par ordre alphabétique des noms et prénoms")

# Randomiser la liste initiale
random.shuffle(liste)